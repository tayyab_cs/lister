// The Flock Model.

class Flock {
  String name;
  var startDay;
  int malesCount;
  int femaleCount;
  int unknownCount;

  Flock(this.name, this.startDay, this.malesCount, this.femaleCount,
      this.unknownCount);

  String get totalDays {
    var diff = (new DateTime.now()).difference(startDay).inDays;
    int week = diff ~/ 7;
    int days = (diff % 7);
    return 'Weeks: $week & $days Days | Actual Days: $diff';
  }

  int get totalBirds => malesCount + femaleCount + unknownCount;
}
