import 'package:flutter/material.dart';

class SimpleList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      child: _customList(context),
    );
  }

  // Simple Static ListView
  // Widget _simpleList(BuildContext context) {
  //   return ListView(
  //     children: ListTile.divideTiles(
  //       context: context,
  //       tiles: [
  //         ListTile(
  //           title: Text('Tayyab'),
  //         ),
  //         ListTile(
  //           title: Text('Mahad'),
  //         ),
  //         ListTile(
  //           title: Text('Ali'),
  //         ),
  //       ],
  //     ).toList(),
  //   );
  // }

  Widget _customList(BuildContext context) {
    final titles = [
      'bike',
      'boat',
      'bus',
      'car',
      'railway',
      'run',
      'subway',
      'transit',
      'walk'
    ];

    final icons = [
      Icons.directions_bike,
      Icons.directions_boat,
      Icons.directions_bus,
      Icons.directions_car,
      Icons.directions_railway,
      Icons.directions_run,
      Icons.directions_subway,
      Icons.directions_transit,
      Icons.directions_walk
    ];

    return ListView.builder(
      itemCount: titles.length,
      itemBuilder: (context, index) {
        return Card(
          child: ListTile(
            leading: Icon(icons[index]),
            title: Text(titles[index]),
          ),
        );
      },
    );
  }

  // Widget _formList(BuildContext context) {
  // return ListView.builder(
  //   itemCount: ,
  // );
  // }
}
