// The Class
class User {
  final name;
  User(this.name);
}

// The Repo.
class UserRepo {
  List<User> users = new List<User>();

  List<User> get getUser => users;

  // static final UserRepo _repo = new UserRepo._internal();

  // factory UserRepo() => _repo;

  UserRepo() {
    // Internal Logic.
    for (int i = 0; i < 10; i++) {
      users.add(User('User # $i'));
    }
  }

  List<User> demiUser() {
    var users_1 = List<User>();
    for (int i = 0; i < 10; i++) {
      users_1.add(User('User # $i'));
    }
    return users_1;
  }
}
