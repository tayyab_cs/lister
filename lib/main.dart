import 'package:flutter/material.dart';
import 'package:lister/flock_ropo.dart';
import 'package:lister/just_repo.dart';
import 'package:lister/simple_list.dart';

import 'flock_list.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Lister',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: new HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        leading: new IconButton(
          icon: const Icon(
            Icons.view_list,
            color: Colors.white,
          ),
          onPressed: null,
        ),
        title: new Container(
          alignment: Alignment.center,
          child: new Text('Lister'),
        ),
        actions: <Widget>[
          new IconButton(
            icon: const Icon(
              Icons.settings,
              color: Colors.white,
            ),
            onPressed: null,
          ),
        ],
      ),
      body: new Container(
          padding: const EdgeInsets.all(20.0),
          decoration: new BoxDecoration(
            color: Colors.white10,
          ),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              _customdate(),
              _getUsers(),
              Expanded(
                child: FlockList(),
              ),
            ],
          )),
      floatingActionButton: new FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: null,
      ),
    );
  }

  Widget _customdate() {
    final dateOne = new DateTime(2019, 05, 01);
    final dateTwo = new DateTime.now();
    final diff = dateTwo.difference(dateOne).inDays;

    int week = diff ~/ 7;
    int days = (diff % 7);

    return new Container(
      padding: EdgeInsets.all(20.0),
      alignment: Alignment.center,
      child: new Text('Weeks: $week & $days Days | Actual Days: $diff'),
    );
  }

  // Widget _getFlock() {
  //   var _myrepo = new FlockRepository();
  //   var _myflock = _myrepo.mflockList;

  //   return new Container(
  //     padding: EdgeInsets.all(20.0),
  //     alignment: Alignment.center,
  //     child: new Text('The Items are ${_myflock.length}'),
  //   );
  // }

  Widget _getUsers() {
    var _repo = new UserRepo();
    var _users = _repo.getUser;
    return new Container(
      padding: EdgeInsets.all(20.0),
      alignment: Alignment.center,
      child: new Text('The Items are ${_users.length}'),
    );
  }
}
