import 'package:flutter/material.dart';

import 'flock_model.dart';
import 'flock_ropo.dart';

class FlockList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      child: _customList(context),
    );
  }

  Widget _customList(BuildContext context) {
    FlockRepository repo = new FlockRepository();
    List<Flock> _flockList = repo.mflockList;

    return ListView.builder(
      itemCount: _flockList.length,
      itemBuilder: (context, index) {
        return Card(
          child: ListTile(
            title: Text(_flockList[index].name),
          ),
        );
      },
    );
  }
}
