// Flock Repo

import 'package:lister/flock_model.dart';

class FlockRepository {
  List<Flock> mflockList = dumiflock();
  List<Flock> get allItems => (mflockList.isEmpty) ? dumiflock() : mflockList;

  static final FlockRepository _repository = new FlockRepository._internal();

  factory FlockRepository() => _repository;

  FlockRepository._internal() {
    // Initialize logic Here.
    mflockList = dumiflock();
  }

  static List<Flock> dumiflock() {
    List<Flock> _mylist = new List();
    _mylist.add(new Flock(
      'Flock # 1',
      new DateTime(2019, 06, 01),
      5,
      5,
      10,
    ));

    _mylist.add(new Flock(
      'Flock # 2',
      new DateTime(2019, 06, 01),
      5,
      5,
      10,
    ));

    _mylist.add(new Flock(
      'Flock # 3',
      new DateTime(2019, 05, 01),
      15,
      25,
      10,
    ));
    return _mylist;
  }

  // Rest of the Code.

}
